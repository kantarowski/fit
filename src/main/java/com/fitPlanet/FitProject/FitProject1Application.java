package com.fitPlanet.FitProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FitProject1Application {

	public static void main(String[] args) {
		SpringApplication.run(FitProject1Application.class, args);
	}

}
